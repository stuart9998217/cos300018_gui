package gui;

import java.awt.*;
import javax.swing.*;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Gui extends JFrame
{

	private JFrame frame;
	
	// button labels
	private JButton btnStart;
	private JButton btnPause;
	private JButton btnQuit;
	private JButton btnRestart;
	
	// image labels
	private JLabel lblAircon = new JLabel();
	private JLabel lblFridge = new JLabel();
	private JLabel lblToaster = new JLabel();
	private JLabel lblTv = new JLabel();
	private JLabel lblCar = new JLabel();
	private JLabel lblRetailer1 = new JLabel();
	private JLabel lblRetailer2 = new JLabel();
	private JLabel lblRetailer3 = new JLabel();
	private JLabel lblBattery = new JLabel();
	private JLabel lblHome = new JLabel();
	
	// text labels
	private JLabel lblAirconText = new JLabel();
	private JLabel lblFridgeText = new JLabel();
	private JLabel lblToasterText = new JLabel();
	private JLabel lblTvText = new JLabel();
	private JLabel lblCarText = new JLabel();
	private JLabel lblRetailer1Text = new JLabel();
	private JLabel lblRetailer2Text = new JLabel();
	private JLabel lblRetailer3Text = new JLabel();
	private JLabel lblBatteryText = new JLabel();
	private JLabel lblTimeText = new JLabel();
	private JLabel lblDateText = new JLabel();
	private JLabel lblBoughtText = new JLabel();
	private JLabel lblSoldText = new JLabel();
	private JLabel lblUsedText = new JLabel();
	private JLabel lblSavedText = new JLabel();
	
	// text
	private String boughtText;
	private String soldText;
	private String usedText;
	private String savedText;
	private String timeText;
	private String dateText;
	private String airconText;
	private String fridgeText;
	private String toasterText;
	private String tvText;
	private String carText;
	private String retailer1Text;
	private String retailer2Text;
	private String retailer3Text;
	private String batteryText;
	
	private double bought;
	private double sold;
	private double used;
	private double saved;
	
	//time fields
	private int minutes;
	private int hours;
	
	// date fields
	private int day;
	private int month;
	private int year;
	
	// aircon fields
	private double airconCurrentUsage;
	private double airconTurnOffTime;
	private double airconExpectedUsage;
	
	// fridge fields
	private double fridgeCurrentUsage;
	private double fridgeTurnOffTime;
	private double fridgeExpectedUsage;
	
	// toaster fields
	private double toasterCurrentUsage;
	private double toasterTurnOffTime;
	private double toasterExpectedUsage;
	
	// tv fields
	private double tvCurrentUsage;
	private double tvTurnOffTime;
	private double tvExpectedUsage;
	
	// car fields
	private double carCurrentUsage;
	private double carTurnOffTime;
	private double carExpectedUsage;

	// retailer1 fields
	private double retailer1SellingPrice;
	private double retailer1BuyingPrice;
	
	// retailer1 fields
	private double retailer2SellingPrice;
	private double retailer2BuyingPrice;

	// retailer1 fields
	private double retailer3SellingPrice;
	private double retailer3BuyingPrice;
	
	// battery fields
	private double elecStored = 0;
	private JScrollPane scrollBar;
	
	// battery panel
	private JPanel battery3SbPanel = new JPanel();
	private JPanel batteryColor = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui window = new Gui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Gui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		// create window
		frame = new JFrame();
		frame.setBounds(100, 100, 1000, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		/**
		 *  handles all of the button functionality
		 */
		System.out.print("hi");
		btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnStart.setBounds(6, 484, 89, 38);
		frame.getContentPane().add(btnStart);
		
		btnPause = new JButton("Pause");
		btnPause.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnPause.setBounds(96, 484, 89, 38);
		frame.getContentPane().add(btnPause);
		
		btnQuit = new JButton("Quit");
		btnQuit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnQuit.setBounds(280, 484, 89, 38);
		frame.getContentPane().add(btnQuit);
		
		btnRestart = new JButton("Restart");
		btnRestart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnRestart.setBounds(187, 484, 89, 38);
		frame.getContentPane().add(btnRestart);
		
		// set panel
		setPanel();
				
		//set time and date
		setTime();
		setDate();
		
		/**
		 *  handles all of the home functionality
		 */
		
		lblHome = new JLabel("");
		Image home = new ImageIcon(this.getClass().getResource("/gui/icons/home.png")).getImage();
		lblHome.setIcon(new ImageIcon(home));
		lblHome.setBounds(431, 157, 150, 157);
		frame.getContentPane().add(lblHome);
		
		/**
		 *  handles all of the aircon functionality
		 */
		
		// sets the aircon image
		lblAircon = new JLabel("");
		Image aircon = new ImageIcon(this.getClass().getResource("/gui/icons/aircon.png")).getImage();
		lblAircon.setIcon(new ImageIcon(aircon));
		lblAircon.setBounds(22, 13, 97, 76);
		frame.getContentPane().add(lblAircon);
		
		setAirconData();
		
		/**
		 *  handles all of the fridge functionality
		 */
		
		// sets the fridge image
		lblFridge = new JLabel("");
		Image fridge = new ImageIcon(this.getClass().getResource("/gui/icons/fridge.png")).getImage();
		lblFridge.setIcon(new ImageIcon(fridge));
		lblFridge.setBounds(22, 104, 97, 93);
		frame.getContentPane().add(lblFridge);
		
		setFridgeData();
		
		/**
		 *  handles all of the toaster functionality
		 */
		
		// sets the toaster image
		lblToaster = new JLabel("");
		Image toaster = new ImageIcon(this.getClass().getResource("/gui/icons/toaster.png")).getImage();
		lblToaster.setIcon(new ImageIcon(toaster));
		lblToaster.setBounds(22, 209, 97, 93);
		frame.getContentPane().add(lblToaster);
		
		setToasterData();
		
		/**
		 *  handles all of the tv functionality
		 */
		
		// sets the tv image
		lblTv = new JLabel("");
		Image tv = new ImageIcon(this.getClass().getResource("/gui/icons/tv.png")).getImage();
		lblTv.setIcon(new ImageIcon(tv));
		lblTv.setBounds(22, 327, 97, 68);
		frame.getContentPane().add(lblTv);
		
		setTvData();
		
		/**
		 *  handles all of the car functionality
		 */
		
		// sets the car image
		lblCar = new JLabel("");
		Image car = new ImageIcon(this.getClass().getResource("/gui/icons/car.png")).getImage();
		lblCar.setIcon(new ImageIcon(car));
		lblCar.setBounds(22, 418, 97, 48);
		frame.getContentPane().add(lblCar);
		
		setCarData();
		
		/**
		 *  handles all of the retailer functionality
		 */
		
		// sets the retailer1 image
		lblRetailer1 = new JLabel("");
		Image retailer = new ImageIcon(this.getClass().getResource("/gui/icons/retailer.png")).getImage();
		lblRetailer1.setIcon(new ImageIcon(retailer));
		lblRetailer1.setBounds(885, 13, 97, 93);
		frame.getContentPane().add(lblRetailer1);
		
		// sets the retailer2 image
		lblRetailer2 = new JLabel("");
		lblRetailer2.setIcon(new ImageIcon(retailer));
		lblRetailer2.setBounds(885, 104, 97, 93);
		frame.getContentPane().add(lblRetailer2);
		
		// sets the retailer3 image
		lblRetailer3 = new JLabel("");
		lblRetailer3.setIcon(new ImageIcon(retailer));
		lblRetailer3.setBounds(885, 195, 97, 93);
		frame.getContentPane().add(lblRetailer3);
		
		// set retailer data
		setRetailer1Data();
		setRetailer2Data();
		setRetailer3Data();
		
		/**
		 *  handles all of the battery functionality
		 */
		
		// sets the battery image
		lblBattery = new JLabel("");
		Image battery = new ImageIcon(this.getClass().getResource("/gui/icons/battery.png")).getImage();
		lblBattery.setIcon(new ImageIcon(battery));
		lblBattery.setBounds(885, 300, 97, 68);
		frame.getContentPane().add(lblBattery);
		
		setBatteryData();
	}
	
	/**
	 *  handles all of the right-bottom panel presentation
	 */
	public void setPanel()
	{	
		// set bought electricity text
		boughtText = "Bought (kWs): " + bought;
		
		lblBoughtText.setText(boughtText);
		lblBoughtText.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		lblBoughtText.setForeground(Color.BLACK);
		lblBoughtText.setBounds(724, 406, 275, 31);
		frame.getContentPane().add(lblBoughtText);
		
		// set sold electricity text
		soldText = "    Sold (kWs): " + sold;
		
		lblSoldText.setText(soldText);
		lblSoldText.setForeground(Color.BLACK);
		lblSoldText.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		lblSoldText.setBounds(726, 435, 272, 31);
		frame.getContentPane().add(lblSoldText);
		
		// set used electricity text
		usedText = "   Used (kWs): " + used;
		
		lblUsedText.setText(usedText);
		lblUsedText.setForeground(Color.BLACK);
		lblUsedText.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		lblUsedText.setBounds(726, 463, 272, 31);
		frame.getContentPane().add(lblUsedText);
		
		// set saved $ text
		savedText = "      Saved ($): " + saved;
		
		lblSavedText.setText(savedText);
		lblSavedText.setForeground(Color.BLACK);
		lblSavedText.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		lblSavedText.setBounds(726, 490, 272, 31);
		frame.getContentPane().add(lblSavedText);
	}
	
	/**
	 *  handles all of the time presentation
	 */
	public void setTime()
	{
		
		// sets the time text
		timeText = "Time:" + hours + ":" + minutes;
		
		lblTimeText.setText(timeText);
		lblTimeText.setForeground(Color.BLACK);
		lblTimeText.setBounds(396, 484, 81, 16);
		frame.getContentPane().add(lblTimeText);
		revalidate();
		repaint();
	}
	
	/**
	 *  handles all of the date presentation
	 */
	public void setDate()
	{
		// sets the date text
		dateText = "Date:" + day + "." + month + "." + year;
		
		lblDateText.setText(dateText);
		lblDateText.setForeground(Color.BLACK);
		lblDateText.setBounds(396, 503, 115, 16);
		frame.getContentPane().add(lblDateText);
	}
	
	/**
	 *  handles all of the aircon data presentation
	 */	
	public void setAirconData()
	{
		// sets the aircon text
		airconText = "<html>   Current usage: " + airconCurrentUsage + 
					 "<br>Exp. turn off time: " + airconTurnOffTime +
					 "<br>        Exp. usage: " + airconExpectedUsage;
				
		lblAirconText.setText(airconText);
		lblAirconText.setForeground(Color.RED);
		lblAirconText.setVerticalAlignment(SwingConstants.TOP);
				
		// stores the aircon text in a scrollbar
		JPanel airconSbPanel = new JPanel();
		scrollBar = new JScrollPane(airconSbPanel,
					            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
					            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollBar.setBounds(130, 13, 200, 55);
		scrollBar.setViewportView(lblAirconText);
		frame.getContentPane().add(scrollBar);
	}

	/**
	 *  handles all of the fridge data presentation
	 */	
	public void setFridgeData()
	{
		// sets fridge text
		fridgeText = new String ("<html>   Current usage: " + fridgeCurrentUsage + 
						 				 "<br>Exp. turn off time: " + fridgeTurnOffTime +
						 				 "<br>        Exp. usage: " + fridgeExpectedUsage);
		lblFridgeText = new JLabel(fridgeText);
		lblFridgeText.setForeground(Color.RED);
		lblFridgeText.setVerticalAlignment(SwingConstants.TOP);
				
		// stores the fridge text in a scroll bar
		JPanel fridgeSbPanel = new JPanel();
		scrollBar = new JScrollPane(fridgeSbPanel,
			            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollBar.setBounds(130, 120, 200, 55);
		scrollBar.setViewportView(lblFridgeText);
		frame.getContentPane().add(scrollBar);
	}
	
	/**
	 *  handles all of the toaster data presentation
	 */	
	public void setToasterData()
	{
		// sets toaster text
		toasterText = new String ("<html>   Current usage: " + toasterCurrentUsage + 
						 "<br>Exp. turn off time: " + toasterTurnOffTime +
						 "<br>        Exp. usage: " + toasterExpectedUsage);
		lblToasterText = new JLabel(toasterText);
		lblToasterText.setForeground(Color.RED);
		lblToasterText.setVerticalAlignment(SwingConstants.TOP);
				
		// stores the toaster text in a scroll bar
		JPanel toasterSbPanel = new JPanel();
		scrollBar = new JScrollPane(toasterSbPanel,
			            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollBar.setBounds(130, 230, 200, 55);
		scrollBar.setViewportView(lblToasterText);
		frame.getContentPane().add(scrollBar);
	}
	
	/**
	 *  handles all of the toaster data presentation
	 */	
	public void setTvData()
	{
		//sets the tv text
		tvText = new String ("<html>   Current usage: " + tvCurrentUsage + 
						 	 "<br>Exp. turn off time: " + tvTurnOffTime +
						 	 "<br>        Exp. usage: " + tvExpectedUsage);
		lblTvText = new JLabel(tvText);
		lblTvText.setForeground(Color.RED);
		lblTvText.setVerticalAlignment(SwingConstants.TOP);
				
		// stores the tv text in a scroll bar
		JPanel tvSbPanel = new JPanel();
		scrollBar = new JScrollPane(tvSbPanel,
			            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollBar.setBounds(130, 330, 200, 55);
		scrollBar.setViewportView(lblTvText);
		frame.getContentPane().add(scrollBar);
	}
	
	/**
	 *  handles all of the toaster data presentation
	 */	
	public void setCarData()
	{
		// sets the car text
		carText = new String ("<html>   Current usage: " + carCurrentUsage + 
							  "<br>Exp. turn off time: " + carTurnOffTime +
							  "<br>        Exp. usage: " + carExpectedUsage);
		lblCarText = new JLabel(carText);
		lblCarText.setForeground(Color.RED);
		lblCarText.setVerticalAlignment(SwingConstants.TOP);
				
		// stores the car text in a scroll bar
		JPanel carSbPanel = new JPanel();
		scrollBar = new JScrollPane(carSbPanel,
			            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollBar.setBounds(130, 410, 200, 55);
		scrollBar.setViewportView(lblCarText);
		frame.getContentPane().add(scrollBar);
	}
	
	/**
	 *  handles all of the retailer1 data presentation
	 */	
	public void setRetailer1Data()
	{
		//sets the retailer1 text
		retailer1Text = new String ("<html>Selling at $" + retailer1SellingPrice + " / kWs" +
											"<br>Buying at $" + retailer1BuyingPrice + " / kWs");
		lblRetailer1Text = new JLabel(retailer1Text);
		lblRetailer1Text.setForeground(Color.RED);
		lblRetailer1Text.setVerticalAlignment(SwingConstants.TOP);
				
		// stores the retailer1 text in a scrollbar
		JPanel retailer1SbPanel = new JPanel();
		scrollBar = new JScrollPane(retailer1SbPanel,
			            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollBar.setBounds(670, 30, 200, 55);
		scrollBar.setViewportView(lblRetailer1Text);
		frame.getContentPane().add(scrollBar);
	}
	
	/**
	 *  handles all of the retailer1 data presentation
	 */	
	public void setRetailer2Data()
	{
		// sets the retailer2 text
		retailer2Text = new String ("<html>Selling at $" + retailer2SellingPrice +" / kWs" +
											"<br>Buying at $" + retailer2BuyingPrice + " / kWs");
		lblRetailer2Text = new JLabel(retailer2Text);
		lblRetailer2Text.setForeground(Color.RED);
		lblRetailer2Text.setVerticalAlignment(SwingConstants.TOP);
				
		// stores the retailer2 text in a scrollbar
		JPanel retailer2SbPanel = new JPanel();
		scrollBar = new JScrollPane(retailer2SbPanel,
			            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollBar.setBounds(670, 120, 200, 55);
		scrollBar.setViewportView(lblRetailer2Text);
		frame.getContentPane().add(scrollBar);
	}
	
	public void setRetailer3Data()
	{
		// sets the retailer3 text
		retailer3Text = new String ("<html>Selling at $" + retailer3SellingPrice +" / kWs" +
											"<br>Buying at $" + retailer3BuyingPrice + " / kWs");
		lblRetailer3Text = new JLabel(retailer3Text);
		lblRetailer3Text.setForeground(Color.RED);
		lblRetailer3Text.setVerticalAlignment(SwingConstants.TOP);
				
		// stores the retailer3 text in a scrollbar
		JPanel retailer3SbPanel = new JPanel();
		scrollBar = new JScrollPane(retailer3SbPanel,
			            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollBar.setBounds(670, 210, 200, 55);
		scrollBar.setViewportView(lblRetailer3Text);
		frame.getContentPane().add(scrollBar);
	}
	
	public void setBatteryData()
	{
		//sets the battery text
		batteryText = new String ("<html>" + elecStored + " kWs stored of<br>a 10 kW capacity");
		lblBatteryText = new JLabel(batteryText);
		lblBatteryText.setForeground(Color.RED);
		lblBatteryText.setVerticalAlignment(SwingConstants.TOP);
				
		// stores the battery text in a scrollbar
		scrollBar = new JScrollPane(battery3SbPanel,
			            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollBar.setBounds(670, 310, 200, 55);
		scrollBar.setViewportView(lblBatteryText);
		frame.getContentPane().add(scrollBar);
				
		// animates the battery color
		int storedPercent = animateBattery(batteryColor);
				System.out.print(storedPercent);
		
		batteryColor.setBounds(685, 310 + (58 - storedPercent), 81, storedPercent);
		frame.getContentPane().add(batteryColor);
	}
	
	public int animateBattery(JPanel batteryColor)
	{
		double boxPercent;
		int batteryPercent;
		
		boxPercent = (58.0/100);
		batteryPercent = (int) Math.round(elecStored * 10.0);
		setBatteryColor(batteryPercent, batteryColor);
		
		return (int) Math.round(boxPercent * batteryPercent);
	}
	
	public void setBatteryColor(int stored, JPanel batteryColor)
	{
		if (stored < 25)
		{
			batteryColor.setBackground(Color.RED);
		}
		else if (stored < 75)
		{
			batteryColor.setBackground(Color.YELLOW);
		}
		else
		{
			batteryColor.setBackground(Color.GREEN);
		}
	}
}

		
		

